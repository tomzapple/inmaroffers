//
//  MyOffersTableViewController.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 20/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOffersTableViewController : UITableViewController
@property (nonatomic, strong) NSDictionary *optedOffer;
@end
