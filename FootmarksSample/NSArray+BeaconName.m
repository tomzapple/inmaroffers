//
//  NSArray+BeaconName.m
//  FootmarksSample
//
//  Created by Thomson Varghese on 15/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "NSArray+BeaconName.h"
#import "BeaconManager.h"
#import "Footmarks_SDK.h"
#import "NSString+BeaconName.h"

@implementation NSArray (BeaconName)
- (NSArray *) beaconsWithTheirNames{
    NSMutableArray *mutArray = [[NSMutableArray alloc] init];
    for (FMBeacon *beaconObj in self) {
        NSMutableDictionary *beaconDict = [[NSMutableDictionary alloc] init];
        beaconDict[@"FMbeacon"] = beaconObj;
        beaconDict[@"name"] = [beaconObj.macAddress beaconNameFromMACAddress];
        [mutArray addObject:beaconDict];
    }
    return mutArray;
}
@end
