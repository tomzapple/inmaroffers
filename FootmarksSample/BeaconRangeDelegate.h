//
//  BeaconRangeDelegate.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 15/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol BeaconRangeDelegate <NSObject>
- (void) didRangeNewBeaconForOfferCategory:(NSString*)offerCategory;
@end
