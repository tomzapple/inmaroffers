//
//  ShowOfferDetailViewController.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 22/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowOfferDetailViewController : UIViewController
@property (nonatomic,strong) NSDictionary *offerDict;
@end
