//
//  OffersManager.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 27/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ShowOfferDelegate;
@protocol OfferOptedDelegate;
@protocol BarcodeScanOfferFetchDelegate;
@class Barcode;

@interface OffersManager : NSObject

+ (instancetype) sharedInstance;

+ (instancetype) alloc  __attribute__((unavailable("alloc not available, call sharedInstance instead")));
- (instancetype) init   __attribute__((unavailable("init not available, call sharedInstance instead")));
+ (instancetype) new    __attribute__((unavailable("new not available, call sharedInstance instead")));

- (void) startRangingBeaconsForOffers;
- (void) stopRangingBeaconsForOffers;
- (void) fetchOffersForBarCode:(Barcode *)bc;

@property (weak) id<ShowOfferDelegate> showOfferDelegate;
@property (weak) id<OfferOptedDelegate> offerOptedDelegate;
@property (weak) id<BarcodeScanOfferFetchDelegate> barcodeScanOfferFetchDelegate;

@end
