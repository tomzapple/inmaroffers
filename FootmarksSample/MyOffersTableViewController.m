//
//  MyOffersTableViewController.m
//  FootmarksSample
//
//  Created by Thomson Varghese on 20/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "MyOffersTableViewController.h"
#import "ShowOfferDelegate.h"
#import "ShowOfferAlertViewController.h"
#import "ShowOfferDetailViewController.h"
#import "Constants.h"
#import "OffersManager.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface MyOffersTableViewController ()<ShowOfferDelegate>
@property (nonatomic, strong) NSMutableArray *myOffers;

@property (nonatomic,strong) NSDictionary *currentOffer;

@end

@implementation MyOffersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _myOffers = [[NSMutableArray alloc] initWithCapacity:20];
    
    [OffersManager sharedInstance].showOfferDelegate = self;
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_myOffers count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Offer Cell" forIndexPath:indexPath];
    NSDictionary *offer = _myOffers[indexPath.row];
    NSString *description = [NSString stringWithFormat:@"%@ on %@ %@",offer[@"valueText"],offer[@"brand"],offer[@"description"]];
    cell.textLabel.text = description ;
    [cell.imageView setImageWithURL:[NSURL URLWithString:offer[@"imageUrl"]] placeholderImage:nil];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma ShowOfferDelegate Methods
- (void)shouldShowOffer:(NSDictionary*) dict{
    NSLog(@"--------------SHOW OFFER---------------- %@", dict);
    _currentOffer = dict;
    [self performSegueWithIdentifier:@"Show Offer Alert" sender:self];
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    id destinationVC = segue.destinationViewController;
    if([segue.identifier isEqualToString:@"Show Offer Alert"]){
        if ([segue.destinationViewController isKindOfClass:[ShowOfferAlertViewController class]]) {
            ShowOfferAlertViewController *showOffersVC = (ShowOfferAlertViewController *)destinationVC;
            showOffersVC.offerDict = _currentOffer;
            
        }
    }else if ([segue.identifier isEqualToString:@"Show Offer Detail"]){
        if ([destinationVC isKindOfClass:[ShowOfferDetailViewController class]]){
            
            ShowOfferDetailViewController *showDetailVC = (ShowOfferDetailViewController *)destinationVC;
            showDetailVC.offerDict = self.myOffers[indexPath.row];
        }
        
    }
    
}

//unwinding seague action
- (IBAction)optedForOffer:(UIStoryboardSegue*)sender{
    NSLog(@"--------optedForOffer----------");
    if (_currentOffer != nil) {
        [_myOffers addObject:_currentOffer];
        [self.tableView reloadData];
    }
    
}

- (IBAction)didNotOptForOffer:(UIStoryboardSegue*)sender{
    NSLog(@"-------- didNotOptForOffer--------");
    
}



@end
