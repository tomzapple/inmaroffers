//
//  NSString+BeaconName.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 15/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (BeaconName)
-(NSString *) beaconNameFromMACAddress;
@end
