//
//  MyOffersCollectionViewController.h
//  InmarOffers
//
//  Created by Thomson Varghese on 29/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOffersCollectionViewController : UICollectionViewController
@property (nonatomic, strong) NSDictionary *optedOffer;
@end
