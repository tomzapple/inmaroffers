//
//  MyOffersCollectionViewController.m
//  InmarOffers
//
//  Created by Thomson Varghese on 29/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "MyOffersCollectionViewController.h"
#import "ShowOfferDetailViewController.h"
#import "Constants.h"
#import "OfferOptedDelegate.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "NSString+DateAdditions.h"
#import "OffersManager.h"

@interface MyOffersCollectionViewController ()<OfferOptedDelegate>

@property (nonatomic, strong) NSMutableArray *myOffers;
@property (nonatomic,strong) NSDictionary *currentOffer;

@end

@implementation MyOffersCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    [OffersManager sharedInstance].offerOptedDelegate = self;
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    //    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    
    _myOffers = [[NSMutableArray alloc] initWithCapacity:20];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    id destinationVC = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"Show Offer Detail"]){
        if ([destinationVC isKindOfClass:[ShowOfferDetailViewController class]]){
            NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
            ShowOfferDetailViewController *showDetailVC = (ShowOfferDetailViewController *)destinationVC;
            showDetailVC.offerDict = self.myOffers[indexPath.row];
        }
        
    }
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [_myOffers count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *offer = _myOffers[indexPath.row];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:100];
    [imgView setImageWithURL:[NSURL URLWithString:offer[@"imageUrl"]] placeholderImage:nil];
    [(UILabel *)[cell viewWithTag:101] setText:[NSString stringWithFormat:@"%@ on  %@",offer[@"valueText"],offer[@"description"]]];
    [(UILabel *)[cell viewWithTag:102] setText:[offer[@"expirationDate"][@"iso"] getExpiresInDays]];
    
    
    
    return cell;
}

#pragma mark <OfferOptedDelegate>

- (void)didOptOffer:(NSDictionary*)dict{
    
    [self.myOffers addObject:dict];
    [self.collectionView reloadData];
    
}
#pragma mark <UICollectionViewDelegate>

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */






@end
