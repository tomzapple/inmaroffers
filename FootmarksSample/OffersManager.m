//
//  OffersManager.m
//  FootmarksSample
//
//  Created by Thomson Varghese on 27/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

// A Facade which handles Beacon notifier and the ICEAPI

#import "OffersManager.h"
#import "BeaconNotifier.h"
#import "ICEAPI.h"
#import "DeCAAPI.h"
#import "BeaconRangeDelegate.h"
#import "ShowOfferDelegate.h"
#import "OfferOptedDelegate.h"
#import "BarcodeScanOfferFetchDelegate.h"
#import "FootmarksExperienceDelegate.h"
#import "Constants.h"
#import "Barcode.h"
#import <UIKit/UIKit.h>

@interface OffersManager()<BeaconRangeDelegate, FootmarksExperienceDelegate>
@property (nonatomic, strong) ICEAPI *iceAPI;
@end

@implementation OffersManager

+ (instancetype) sharedInstance {
    
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] initInstance];
        
    });
    return sharedInstance;
}

- (instancetype) initInstance {
    self = [super init];
    _iceAPI = [[ICEAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readyToShowOffer:) name:kPostShowOfferNotification object:nil];
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void) startRangingBeaconsForOffers {
    
    [[BeaconNotifier sharedInstance] startRangingBeacons];
    [[BeaconNotifier sharedInstance] setBeaconRangeDelegate:self];
    [[BeaconNotifier sharedInstance] setFmExperienceDelegate:self];
    
}

- (void) stopRangingBeaconsForOffers{
    [[BeaconNotifier sharedInstance] stopRangingBeacons];
    [[BeaconNotifier sharedInstance] setBeaconRangeDelegate:nil];
    [[BeaconNotifier sharedInstance] setFmExperienceDelegate:nil];
    
}

- (void) fetchOffersForBarCode:(Barcode *)bc{
    [DeCAAPI fetchOfferForBarCode:bc completionHandler:^(NSArray *output, NSError *error) {
        [_barcodeScanOfferFetchDelegate didFetchOffer:output[0]];
    }];
    
}


#pragma Beacon Range Delegate methods


- (void)didRangeNewBeaconForOfferCategory:(NSString *)offerCategory{
    NSLog(@"--------------------didRangeNewBeaconForOfferCategory-----------%@", offerCategory);
    
    [_iceAPI fetchOffersForCategory:offerCategory completionHandler:^(NSArray *output, NSError *error) {
        if (output != nil && [output count]) {
            NSDictionary *offer = [output objectAtIndex:[self randomNumberBetween:0 maxNumber:[output count]-1]];
            [self scheduleNotificationWithItem:offer];
        }else {
            //sometimes you don't get offers
            NSLog(@"--------------NO OFFERS-----------------");
        }
    }];
    
}

#pragma FootmarksExperienceDelegate Delegate methods

- (void) didCompleteExperiences:(NSArray *)experiences{
    NSLog(@"------did complete experiences---- %@", experiences);
}


- (NSInteger)randomNumberBetween:(NSInteger)min maxNumber:(NSInteger)max
{
    NSInteger randomNo = max;
    
    if (max != 0) {
        randomNo = min + arc4random() % (max - min);
    }
    
    NSLog(@"------------Random no : %ld", (long)randomNo);
    return randomNo;
}

- (void)scheduleNotificationWithItem:(NSDictionary*) offer {
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    
    NSString *brand;
    NSString *description;
    NSURL *imageURL;
    NSString * valueText;
    
    brand = offer[@"brand"];
    description = offer[@"description"];
    valueText = offer[@"valueText"];
    imageURL = [NSURL URLWithString:offer[@"imageUrl"]];
    
    NSDate *itemDate = [NSDate dateWithTimeIntervalSinceNow:0];
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    
    NSString *alertBody = [NSString stringWithFormat:@"%@ on %@ %@",valueText,brand,description];
    localNotif.alertBody = NSLocalizedString(alertBody,nil);
    localNotif.alertAction = NSLocalizedString(@"Redeem", nil);
    localNotif.alertTitle = NSLocalizedString(valueText,nil);
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    NSDictionary *infoDict;
    
    if (offer != nil) {
        infoDict = [NSDictionary dictionaryWithObject:offer forKey:@"offer"];
        localNotif.userInfo = infoDict;
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

- (void)readyToShowOffer:(NSNotification *)notif{
    
    NSDictionary *offer = [notif object];
    [_showOfferDelegate shouldShowOffer:offer];
    
}


@end
