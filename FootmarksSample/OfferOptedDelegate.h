//
//  OfferOptedDelegate.h
//  InmarOffers
//
//  Created by Thomson Varghese on 30/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OfferOptedDelegate <NSObject>
- (void)didOptOffer:(NSDictionary*)dict;
@end
