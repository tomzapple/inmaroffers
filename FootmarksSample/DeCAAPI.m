//
//  DeCAAPI.m
//  InmarOffers
//
//  Created by Thomson Varghese on 31/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "DeCAAPI.h"
#import "Barcode.h"

@implementation DeCAAPI

+ (void)fetchOfferForBarCode:(Barcode *)barcode completionHandler: (void (^)(NSArray *output, NSError* error))completionBlock{
    NSString * urlString = [NSString stringWithFormat:@"http://ice-pricechopper.dpn.inmar.com/offers?upc=%@",/*[barcode getBarcodeData]*/@"00007124921229"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSURLSessionDownloadTask *task = [session downloadTaskWithRequest: request
                                                    completionHandler:^(NSURL *localfile, NSURLResponse *response, NSError *error) {
                                                        
                                                        NSData *data = [[NSData alloc] initWithContentsOfURL:localfile];
                                                        
                                                        NSArray *resultArray = [NSJSONSerialization
                                                                JSONObjectWithData:data
                                                                                          options:0
                                                                                          error:NULL];
                                                        NSLog(@"-----DeCA Result %@-------",resultArray);
                                                        completionBlock(resultArray,error);
                                                    }];
    [task resume];

}
@end
