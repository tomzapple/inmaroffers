//
//  BeaconManager.m
//  FootmarksSample
//
//  Created by Thomson Varghese on 15/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "BeaconNotifier.h"
#import "Footmarks_SDK.h"

@interface BeaconNotifier()
@property (nonatomic, strong) FMBeacon *closestBeacon;
@property (readonly) NSDictionary *beaconMap;
@property (readonly) NSDictionary *offerMap;
@end
@implementation BeaconNotifier

+ (instancetype) sharedInstance {
    
    // a mapping between of beacon UUIDs to their name
    
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] initInstance];
        
    });
    return sharedInstance;
}

- (instancetype) initInstance {
    self = [super init];

    //build beacon-offer map
    _beaconOfferMap = [[NSMutableDictionary alloc] initWithCapacity:20];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Beacons" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    
   _beaconMap = [NSDictionary dictionaryWithDictionary:[dict objectForKey:@"beaconMap"]];
    _offerMap = [NSDictionary dictionaryWithDictionary:[dict objectForKey:@"offerMap"]];
    
    [_beaconMap enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop) {
        if (_offerMap[value] != nil) {
        _beaconOfferMap[key] = _offerMap[value];
        }

    }];
//    NSLog(@"%@",_beaconOfferMap);
    return self;
}

- (void)setCurrentRangedBeaconRegion:(FMBeaconRegion *)currentRangedBeaconRegion
{
    //the currentBeacon region keeps on getting set when AppDelegate ranges beacons. We need to determine if this is a new beacon
    _currentRangedBeaconRegion = currentRangedBeaconRegion;
    
    
}

- (void)setCurrentRangedBeacons:(NSArray *)currentRangedBeacons{

    //sorting the ranged beacons in the descending order of proximity. This is not known to work well as it was observed that often even if the beacons are nearer, the CLProximity value received from the beacon is CLProximityFar. Still running this sort.
    NSArray *sortedBeacons = [currentRangedBeacons sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        CLProximity first = [(FMBeacon*)a proximity];
        CLProximity second = [(FMBeacon*)b proximity];
        return first < second?first == 0?NSOrderedDescending:NSOrderedAscending:NSOrderedDescending;
    }];
    _currentRangedBeacons = sortedBeacons;
    
    [self setClosestBeacon:sortedBeacons[0]];
    
}


- (void)setLastExitBeaconRegion:(FMBeaconRegion *)lastExitBeaconRegion{
   
    _lastExitBeaconRegion = lastExitBeaconRegion;
   
    if ([_closestBeacon.proximityUUID.UUIDString isEqualToString:lastExitBeaconRegion.proximityUUID.UUIDString]) {
       //no more closest beacon
        _closestBeacon = nil;
    }
}

- (void)setClosestBeacon:(FMBeacon *)closestBeacon{
    
    //check if the beacon ranged is within our scope
    if (_beaconOfferMap[closestBeacon.macAddress] != nil) {
        
        //check if the closest beacon is not the previous one
        if ((_closestBeacon == nil) || ![_closestBeacon.macAddress isEqualToString:closestBeacon.macAddress]) {
            _closestBeacon = closestBeacon;
            NSLog(@"------New Closest Beacon %@-------",_beaconMap[_closestBeacon.macAddress]);
            
            //trigger the delegate with the offer category
            [_beaconRangeDelegate didRangeNewBeaconForOfferCategory:_beaconOfferMap[_closestBeacon.macAddress]];
        }
    }
}

- (void)stopRangingBeacons{
    
    NSError *error;
    [[FMBeaconManager sharedInstance] stopScanningForFMBeaconsWithError:&error];
    
}

- (void)startRangingBeacons {
    
    NSError *error;
    [[FMBeaconManager sharedInstance] startScanningForFMBeaconsWithError:&error];
    
}


@end
