//
//  NSArray+BeaconName.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 15/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>


// a category to pull beacon names from 
@interface NSArray (BeaconName)
- (NSArray *) beaconsWithTheirNames;
@end
