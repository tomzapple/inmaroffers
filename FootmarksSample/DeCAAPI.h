//
//  DeCAAPI.h
//  InmarOffers
//
//  Created by Thomson Varghese on 31/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Barcode;

@interface DeCAAPI : NSObject

+ (void)fetchOfferForBarCode:(Barcode *)barcode completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;

@end
