//
//  NSString+DateAdditions.m
//  InmarOffers
//
//  Created by Thomson Varghese on 29/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "NSString+DateAdditions.h"

@implementation NSString (DateAdditions)
- (NSString *)getExpiresInDays{
    
    NSString *dateString = self;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    // Always use this locale when parsing fixed format date strings
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:posix];
    NSDate *expirationDate = [formatter dateFromString:dateString];
    NSLog(@"date = %@", expirationDate);
    

    
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *components = [gregorian components:unitFlags
                                                fromDate:[NSDate date]
                                                  toDate:expirationDate options:0];
    NSInteger years = [components year];
    NSInteger months = [components month];
    NSInteger days = [components day];
    NSLog(@"years = %ld months = %ld days = %ld",years,months,days);
    NSString *finalDateString = @"";
    
    
    if (years) {
        finalDateString = [NSString stringWithFormat:@"%ld years",years];
    }
    
    if (months) {
        finalDateString = [NSString stringWithFormat:@"%@ %ld months",finalDateString,months];
    }
    if (days) {
        finalDateString = [NSString stringWithFormat:@"%@ %ld days",finalDateString,days];
    }
     return finalDateString;
    
}
@end
