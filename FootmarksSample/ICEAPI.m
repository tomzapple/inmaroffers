//
//  ICEAPI.m
//  FootmarksSample
//
//  Created by Thomson Varghese on 27/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "ICEAPI.h"
#import "SWGOffersApi.h"
#import "SWGApiClient.h"
#import "Constants.h"

@implementation ICEAPI

- (void) fetchOffersForCategory:(NSString *) offerCategory completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock{
    
    SWGApiClient *apiClient = [SWGApiClient sharedClientFromPool:kICEOffersAPI];
    SWGOffersApi *offersAPI = [[SWGOffersApi alloc] initWithApiClient:apiClient];
    
    NSArray *category = @[offerCategory];
    
    [offersAPI getGroupOffersWithCompletionBlock:@"" groups:nil sort:nil categories:category keywords:nil
                                           limit:[NSNumber numberWithInt:20]
                                            skip:[NSNumber numberWithInt:0]
                               completionHandler:completionBlock];
    
}
@end
