//
//  ShowOfferDetailViewController.m
//  FootmarksSample
//
//  Created by Thomson Varghese on 22/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "ShowOfferDetailViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "NSString+DateAdditions.h"

@interface ShowOfferDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *offerImageView;
@property (weak, nonatomic) IBOutlet UILabel *offerDescription;
@property (weak, nonatomic) IBOutlet UILabel *expiresInLabel;
@property (weak, nonatomic) IBOutlet UILabel *minPurchaseLabel;

@end

@implementation ShowOfferDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _offerDict[@"shortDescription"];
    NSString *imageURLString = [_offerDict objectForKey:@"imageUrl"];
    [_offerImageView setImageWithURL:[NSURL URLWithString:imageURLString] placeholderImage:nil];
    _offerDescription.text = [NSString stringWithFormat:@"%@ on %@ %@",_offerDict[@"valueText"],_offerDict[@"brand"],_offerDict[@"description"]];
    
//    _offerDescription.text = _offerDict[@"shortDescription"];
    //NSString *expiresInString = _offerDict[@""]
    
    _minPurchaseLabel.text = _offerDict[@"minPurchase"] ;
    
    NSString *dateString = _offerDict[@"expirationDate"][@"iso"];

    _expiresInLabel.text = [dateString getExpiresInDays];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
