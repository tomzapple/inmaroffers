//
//  TabBarController.m
//  InmarOffers
//
//  Created by Thomson Varghese on 30/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "TabBarController.h"
#import "OffersManager.h"
#import "ShowOfferDelegate.h"
#import "OfferOptedDelegate.h"
#import "MyOffersCollectionViewController.h"
#import "ScannerViewController.h"
#import "ShowOfferAlertViewController.h"

@interface TabBarController ()<ShowOfferDelegate>
@property (nonatomic,strong) NSDictionary *currentOffer;
@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [OffersManager sharedInstance].showOfferDelegate = self;

    
    if ([self.selectedViewController isKindOfClass:[MyOffersCollectionViewController class]]) {
        [[OffersManager sharedInstance] startRangingBeaconsForOffers];
    }else if([self.selectedViewController isKindOfClass:[ScannerViewController class]] ) {
        [[OffersManager sharedInstance] stopRangingBeaconsForOffers];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if ([[item title] isEqualToString:@"Scan"]) {
        //tell Offer Manager not to range for any beacons around
        [[OffersManager sharedInstance] stopRangingBeaconsForOffers];
        
    }else if([[item title] isEqualToString:@"My Offers"]){
        [[OffersManager sharedInstance] startRangingBeaconsForOffers];
    }
}

#pragma ShowOfferDelegate Methods
- (void)shouldShowOffer:(NSDictionary*) dict{
    NSLog(@"--------------SHOW OFFER---------------- %@", dict);
    _currentOffer = dict;
    dispatch_async(dispatch_get_main_queue(), ^{
    [self performSegueWithIdentifier:@"Show Offer Alert" sender:self];
    });

    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    id destinationVC = segue.destinationViewController;
    if([segue.identifier isEqualToString:@"Show Offer Alert"]){
        if ([segue.destinationViewController isKindOfClass:[ShowOfferAlertViewController class]]) {
            
            // always dismiss the offer Alert before showing a new one
            if ([[self presentedViewController] isKindOfClass:[ShowOfferAlertViewController class]]) {
                [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
            }
            
            ShowOfferAlertViewController *showOffersVC = (ShowOfferAlertViewController *)destinationVC;
            showOffersVC.offerDict = _currentOffer;
            
        }
        
    }
}

#pragma unwinding seague Methods

- (IBAction)optedForOffer:(UIStoryboardSegue *)sender{
    NSLog(@"--------optedForOffer----------");
    [(id<OfferOptedDelegate>)[OffersManager sharedInstance].offerOptedDelegate didOptOffer:_currentOffer];
    
    //set the tabItem to the MyOffersViewController
    [self setSelectedIndex:0];

    
}

- (IBAction)didNotOptForOffer:(UIStoryboardSegue*)sender{
    NSLog(@"-------- didNotOptForOffer--------");
    
}


@end
