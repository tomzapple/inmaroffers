//
//  NSString+BeaconName.m
//  FootmarksSample
//
//  Created by Thomson Varghese on 15/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "NSString+BeaconName.h"
#import "BeaconManager.h"

@implementation NSString (BeaconName)
- (NSString *) beaconNameFromMACAddress{
    
    NSDictionary *beaconMap = [[BeaconManager sharedInstance] beaconMap];
    return beaconMap[self];
}
@end
