//
//  TabBarController.h
//  InmarOffers
//
//  Created by Thomson Varghese on 30/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController
@end
