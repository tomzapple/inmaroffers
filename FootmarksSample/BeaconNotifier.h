//
//  BeaconManager.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 15/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

// A class  that can handle any beacons, not just FootMarks Beacons. For now support for just Footmark Beacons

#import <Foundation/Foundation.h>
#import "BeaconRangeDelegate.h"
#import "FootmarksExperienceDelegate.h"

@class FMBeaconRegion;
@interface BeaconNotifier : NSObject

@property (weak, nonatomic) id <BeaconRangeDelegate> beaconRangeDelegate;
@property (weak, nonatomic) id <FootmarksExperienceDelegate> fmExperienceDelegate;
@property (readonly) NSMutableDictionary *beaconOfferMap;

@property (nonatomic, strong) FMBeaconRegion *currentRangedBeaconRegion;
@property (nonatomic, strong) NSArray *currentRangedBeacons;
@property (nonatomic, strong) FMBeaconRegion *lastExitBeaconRegion;

- (void)startRangingBeacons;
- (void)stopRangingBeacons;


+ (instancetype) sharedInstance;

+ (instancetype) alloc  __attribute__((unavailable("alloc not available, call sharedInstance instead")));
- (instancetype) init   __attribute__((unavailable("init not available, call sharedInstance instead")));
+ (instancetype) new    __attribute__((unavailable("new not available, call sharedInstance instead")));

@end
