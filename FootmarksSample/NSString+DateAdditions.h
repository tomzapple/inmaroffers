//
//  NSString+DateAdditions.h
//  InmarOffers
//
//  Created by Thomson Varghese on 29/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DateAdditions)
- (NSString *)getExpiresInDays;
@end
