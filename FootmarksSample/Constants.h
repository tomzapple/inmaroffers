//
//  Constants.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 22/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>

//Footmarks Constants
FOUNDATION_EXPORT NSString *const kFootmarksAPIKey;
FOUNDATION_EXPORT NSString *const kFootmarksAppSecret;
FOUNDATION_EXPORT NSString *const kFootmarksUsedId;

FOUNDATION_EXPORT NSString *const kICEOffersAPI;

//Notifications
FOUNDATION_EXPORT NSString *const kPostShowOfferNotification;

