//
//  ICEAPI.h
//  FootmarksSample
//
//  Created by Thomson Varghese on 27/07/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICEAPI : NSObject

- (void) fetchOffersForCategory:(NSString *) offerCategory completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;

@end
