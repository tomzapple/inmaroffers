//
//  AppDelegate.m
//  FootmarksSample
//
//  Created by Thomson on 7/8/15.
//  Copyright (c) 2015 INMAR. All rights reserved.
//

#import "AppDelegate.h"
#import "Footmarks_SDK.h"
#import "BeaconNotifier.h"
#import "Constants.h"
#import "OffersManager.h"


//Footmarks Docs say the following
//Configuring the FMBeaconManagerDelegate and FMExperienceManagerDelegate in the AppDelegate is important because it allows the app to always receive experiences (i.e. callbacks) when the app is not running.
@interface AppDelegate ()< FMBeaconManagerDelegate, FMExperienceManagerDelegate, FMAccountDelegate>
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [(FMAccount *)[FMAccount sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    [[FMBeaconManager sharedInstance] setDelegate:self];
    [[FMExperienceManager sharedInstance] setDelegate:self];
    
    [[FMAccount sharedInstance] setAccountDelegate:self];
    [[FMAccount sharedInstance] loginToFootmarksServer:kFootmarksAPIKey andAppSecret:kFootmarksAppSecret andUserId:kFootmarksUsedId];
    
    //register for notifications
    UIUserNotificationType types = UIUserNotificationTypeBadge |
    UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    UIUserNotificationSettings *mySettings =
    [UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];

    return YES;
}

- (void) application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandle
{
    
    NSDictionary *itemDict = [notification.userInfo objectForKey:@"offer"];
    
    NSLog(@"--------------CALLED FROM BACKGROUND----------------");

    [[NSNotificationCenter defaultCenter] postNotificationName:kPostShowOfferNotification
                                                        object:itemDict
                                                      userInfo:nil];
    
    application.applicationIconBadgeNumber = notification.applicationIconBadgeNumber - 1;
    
}

//docs mention The notification is delivered when the app is running in the foreground.
- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif {
    NSDictionary *itemDict = [notif.userInfo objectForKey:@"offer"];
    
    NSLog(@"--------------CALLED FROM FOREGROUND----------------");

    [[NSNotificationCenter defaultCenter] postNotificationName:kPostShowOfferNotification
                                                        object:itemDict
                                                      userInfo:nil];
    app.applicationIconBadgeNumber = notif.applicationIconBadgeNumber - 1;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



#pragma FMBeaconManager delegate methods

//delegating all beacon events to Beacon Manager
-(void)bluetoothDidSwitchState:(CBCentralManagerState)state{
    NSLog(@"%@ %ld", NSStringFromSelector(_cmd),(long)state);
}

-(void) locationServicesFailedWithError:(NSError *)error{
    NSLog(@"%@ %@", NSStringFromSelector(_cmd), error);
}

- (void) didCompleteExperiences:(NSArray *)experiences
{
    [[[BeaconNotifier sharedInstance] fmExperienceDelegate] didCompleteExperiences:experiences];
}


- (void)beaconManager:(FMBeaconManager *)manager
      didRangeBeacons:(NSArray *)beacons inRegion:(FMBeaconRegion *)region
{

    [BeaconNotifier sharedInstance].currentRangedBeacons = beacons;
    [BeaconNotifier sharedInstance].currentRangedBeaconRegion = region;
}

- (void)beaconManager:(FMBeaconManager *)manager didExitRegion:(FMBeaconRegion *)region{
    [BeaconNotifier sharedInstance].lastExitBeaconRegion = region;
}

#pragma FootmarksAccountDelegateMethods

- (void) loginSuccessful{
        
    //Start OffersManager which will look out for beacons and pull the appropriate offer from using the ICE API
    [[OffersManager sharedInstance] startRangingBeaconsForOffers];
    
    NSLog(@"%@ ", NSStringFromSelector(_cmd));
    
    
}

- (void) loginUnsuccessful:(NSString *)error{
    
    NSLog(@"%@ %@", NSStringFromSelector(_cmd), error);
    
}








@end
