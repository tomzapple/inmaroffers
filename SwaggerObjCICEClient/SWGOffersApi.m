#import "SWGOffersApi.h"
#import "SWGFile.h"
#import "SWGQueryParamCollection.h"


@interface SWGOffersApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation SWGOffersApi

static NSString * basePath = @"http://ice-spartan.test.dpn.inmar.com";

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        self.apiClient = [SWGApiClient sharedClientFromPool:basePath];
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        if (apiClient) {
            self.apiClient = apiClient;
        }
        else {
            self.apiClient = [SWGApiClient sharedClientFromPool:basePath];
        }
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(SWGOffersApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static SWGOffersApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[SWGOffersApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

+(void) setBasePath:(NSString*)path {
    basePath = path;
}

+(NSString*) getBasePath {
    return basePath;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [SWGApiClient requestQueueSize];
}


/*!
 * Get Offers
 * General offer query. Useful for sorting offers on their attributes, categories and keywords. Also suitable for anonymous users since it does not rely on the user context or analytics.
 * \param xAuthToken Authenticated session hash. See [User - Login](#api-User-Login)
 * \param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 * \param sort Comma separated list of keys to sort by. \n\n**Offer Attribute Sorting:**\nKeys include: `brand`, `clipStartDate`, `clipEndDate`, `value` and `type`. \n\nThese keys sort ascending by default. To sort descending place a dash (minus sign) - before the attribute, like: `sort=value,-type`. This would sort offers by value ascending and type descending. \n\n**Special Sorting:** \nThese provide shortcuts or advanced sorting capabilities. Keys include:\n\n`popular` sort offers by popularity descending (most popular offers first) \n`asc` sort offers oldest to newest \n`desc` sort offers newest to oldest \n\nThese keys sort in a fixed direction. No flags (descending dashes, etc) are needed.
 * \param categories Comma separated list of category keys to filter offers by. \nTo get available category keys, see [Offers - Get Categories](#api-Offers-Get_Categories).
 * \param keywords Comma separated list of keywords to filter offers by. \nTo get available keywords, see [Offers - Get Keywords](#api-Offers-Get_Keywords).
 * \param limit Limit the number of returned results
 * \param skip Skip specific number of offers to return. Used with limit to provide pagination.
 * \returns NSArray*
 */
-(NSNumber*) getGroupOffersWithCompletionBlock: (NSString*) xAuthToken
         groups: (NSArray*) groups
         sort: (NSArray*) sort
         categories: (NSArray*) categories
         keywords: (NSArray*) keywords
         limit: (NSNumber*) limit
         skip: (NSNumber*) skip
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(groups != nil) {
        
        queryParams[@"groups"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: groups format: @"csv"];
        
        
    }
    if(sort != nil) {
        
        queryParams[@"sort"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: sort format: @"csv"];
        
        
    }
    if(categories != nil) {
        
        queryParams[@"categories"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: categories format: @"csv"];
        
        
    }
    if(keywords != nil) {
        
        queryParams[@"keywords"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: keywords format: @"csv"];
        
        
    }
    if(limit != nil) {
        
        queryParams[@"limit"] = limit;
    }
    if(skip != nil) {
        
        queryParams[@"skip"] = skip;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                  
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSObject* d = [dict copy];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}

/*!
 * Get Campaign Offers
 * Specialized query for campaign context analytic ranked offers.
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param campaign Campaign identifier
 * \param availability Specialized parameter
 * \param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 * \param skip Skip specific number of offers to return. Used with limit to provide pagination.
 * \param limit Limit the number of returned results
 * \returns NSArray*
 */
-(NSNumber*) getCampaignOffersWithCompletionBlock: (NSString*) xAuthToken
         campaign: (NSString*) campaign
         availability: (NSNumber*) availability
         groups: (NSArray*) groups
         skip: (NSNumber*) skip
         limit: (NSNumber*) limit
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getCampaignOffers");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers/campaign", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(campaign != nil) {
        
        queryParams[@"campaign"] = campaign;
    }
    if(availability != nil) {
        
        queryParams[@"availability"] = availability;
    }
    if(groups != nil) {
        
        queryParams[@"groups"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: groups format: @"csv"];
        
        
    }
    if(skip != nil) {
        
        queryParams[@"skip"] = skip;
    }
    if(limit != nil) {
        
        queryParams[@"limit"] = limit;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSObject* d = [dict copy];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}

/*!
 * Get Categories
 * Get categories for available offers.
 * \param xAuthToken Authenticated session hash. See [User - Login](#api-User-Login)
 * \returns NSArray*
 */
-(NSNumber*) getOfferCategoriesWithCompletionBlock: (NSString*) xAuthToken
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers/categories", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSObject* d = [dict copy];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}

/*!
 * Clip Offer
 * Clip an offer.
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param _id Offer Id
 * \returns NSObject*
 */
-(NSNumber*) clipOfferWithCompletionBlock: (NSString*) xAuthToken
         _id: (NSNumber*) _id
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling clipOffer");
    
    // verify the required parameter '_id' is set
    NSAssert(_id != nil, @"Missing the required parameter `_id` when calling clipOffer");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers/clip", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    
    formParams[@"_id"] = _id;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"POST" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ? [data copy] : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Get Email Offers
 * Specialized query for 'email' context analytic ranked offers.
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param runId Specialized parameter
 * \param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 * \param skip Skip specific number of offers to return. Used with limit to provide pagination.
 * \param limit Limit the number of returned results
 * \returns NSArray*
 */
-(NSNumber*) getEmailOffersWithCompletionBlock: (NSString*) xAuthToken
         runId: (NSNumber*) runId
         groups: (NSArray*) groups
         skip: (NSNumber*) skip
         limit: (NSNumber*) limit
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getEmailOffers");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers/email", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(runId != nil) {
        
        queryParams[@"runId"] = runId;
    }
    if(groups != nil) {
        
        queryParams[@"groups"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: groups format: @"csv"];
        
        
    }
    if(skip != nil) {
        
        queryParams[@"skip"] = skip;
    }
    if(limit != nil) {
        
        queryParams[@"limit"] = limit;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSObject* d = [dict copy];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}

/*!
 * Get Keywords
 * Get offer keywords.
 * \param xAuthToken Authenticated session hash. See [User - Login](#api-User-Login)
 * \returns NSArray*
 */
-(NSNumber*) getOfferKeywordsWithCompletionBlock: (NSString*) xAuthToken
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers/keywords", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSString* d = [[NSString alloc]initWithString: dict];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}

/*!
 * Get Ranked Offers
 * Get analytic ranked offers ordered by analytic `contextTypes` and grouped by state `groups`.
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param campaign Campaign identifier
 * \param contextTypes Comma separated list of analytic context types to return in that order
 * \param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 * \param skip Skip specific number of offers to return. Used with limit to provide pagination.
 * \param limit Limit the number of returned results
 * \returns NSArray*
 */
-(NSNumber*) getRankedOffersWithCompletionBlock: (NSString*) xAuthToken
         campaign: (NSString*) campaign
         contextTypes: (NSArray*) contextTypes
         groups: (NSArray*) groups
         skip: (NSNumber*) skip
         limit: (NSNumber*) limit
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getRankedOffers");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers/ranked", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(campaign != nil) {
        
        queryParams[@"campaign"] = campaign;
    }
    if(contextTypes != nil) {
        
        queryParams[@"contextTypes"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: contextTypes format: @"csv"];
        
        
    }
    if(groups != nil) {
        
        queryParams[@"groups"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: groups format: @"csv"];
        
        
    }
    if(skip != nil) {
        
        queryParams[@"skip"] = skip;
    }
    if(limit != nil) {
        
        queryParams[@"limit"] = limit;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSObject* d = [dict copy];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}

/*!
 * Get Relevant Offers
 * Specialized query for 'relevant' context analytic ranked offers.
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param relevancyType Specialized parameter
 * \param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 * \param skip Skip specific number of offers to return. Used with limit to provide pagination.
 * \param limit Limit the number of returned results
 * \returns NSArray*
 */
-(NSNumber*) getRelevantOffersWithCompletionBlock: (NSString*) xAuthToken
         relevancyType: (NSString*) relevancyType
         groups: (NSArray*) groups
         skip: (NSNumber*) skip
         limit: (NSNumber*) limit
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getRelevantOffers");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers/relevant", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(relevancyType != nil) {
        
        queryParams[@"relevancyType"] = relevancyType;
    }
    if(groups != nil) {
        
        queryParams[@"groups"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: groups format: @"csv"];
        
        
    }
    if(skip != nil) {
        
        queryParams[@"skip"] = skip;
    }
    if(limit != nil) {
        
        queryParams[@"limit"] = limit;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSObject* d = [dict copy];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}

/*!
 * Get Targeted Offers
 * Specialized query for 'targeted' context analytic ranked offers.
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param runId Specialized parameter
 * \param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 * \param skip Skip specific number of offers to return. Used with limit to provide pagination.
 * \param limit Limit the number of returned results
 * \returns NSArray*
 */
-(NSNumber*) getTargetedOffersWithCompletionBlock: (NSString*) xAuthToken
         runId: (NSNumber*) runId
         groups: (NSArray*) groups
         skip: (NSNumber*) skip
         limit: (NSNumber*) limit
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getTargetedOffers");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/offers/targeted", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(runId != nil) {
        
        queryParams[@"runId"] = runId;
    }
    if(groups != nil) {
        
        queryParams[@"groups"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: groups format: @"csv"];
        
        
    }
    if(skip != nil) {
        
        queryParams[@"skip"] = skip;
    }
    if(limit != nil) {
        
        queryParams[@"limit"] = limit;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSObject* d = [dict copy];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}



@end



