#import <Foundation/Foundation.h>
#import "SWGObject.h"
#import "SWGApiClient.h"


@interface SWGUserApi: NSObject

@property(nonatomic, assign)SWGApiClient *apiClient;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient;
-(void) addHeader:(NSString*)value forKey:(NSString*)key;
-(unsigned long) requestQueueSize;
+(SWGUserApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key;
+(void) setBasePath:(NSString*)basePath;
+(NSString*) getBasePath;
/**

 Get User Info
 Access the information concerning the user account

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 

 return type: NSObject*
 */
-(NSNumber*) getUserInfoWithCompletionBlock :(NSString*) xAuthToken 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Update User Info
 Update user information

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param username Loyalty id or phone number
 @param email User email address
 @param firstName first name
 @param lastName last name
 @param phone telephone number
 @param address1 address line one
 @param address2 address line two
 @param city address city
 @param state address state
 @param zip postal code
 @param emailoptin true if user has opted in for email offers
 @param altid Alternate identifier
 

 return type: NSObject*
 */
-(NSNumber*) saveUserInfoWithCompletionBlock :(NSString*) xAuthToken 
     username:(NSString*) username 
     email:(NSString*) email 
     firstName:(NSString*) firstName 
     lastName:(NSString*) lastName 
     phone:(NSString*) phone 
     address1:(NSString*) address1 
     address2:(NSString*) address2 
     city:(NSString*) city 
     state:(NSString*) state 
     zip:(NSString*) zip 
     emailoptin:(NSString*) emailoptin 
     altid:(NSString*) altid 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Get Clipped Offers
 Get the clipped offers for a user. Essentially the user's digital coupon 'wallet'.

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param sort Comma separated list of keys to sort by. \n\n**Offer Attribute Sorting:**\nKeys include: `brand`, `clipStartDate`, `clipEndDate`, `value` and `type`. \n\nThese keys sort ascending by default. To sort descending place a dash (minus sign) - before the attribute, like: `sort=value,-type`. This would sort offers by value ascending and type descending. \n\n**Special Sorting:** \nThese provide shortcuts or advanced sorting capabilities. Keys include:\n\n`popular` sort offers by popularity descending (most popular offers first) \n`asc` sort offers oldest to newest \n`desc` sort offers newest to oldest \n\nThese keys sort in a fixed direction. No flags (descending dashes, etc) are needed.
 @param categories Comma separated list of category keys to filter offers by. \nTo get available category keys, see [Offers - Get Categories](#api-Offers-Get_Categories).
 @param keywords Comma separated list of keywords to filter offers by. \nTo get available keywords, see [Offers - Get Keywords](#api-Offers-Get_Keywords).
 @param limit Limit the number of returned results
 @param skip Skip specific number of offers to return. Used with limit to provide pagination.
 

 return type: NSArray*
 */
-(NSNumber*) getUserClippedOffersWithCompletionBlock :(NSString*) xAuthToken 
     sort:(NSArray*) sort 
     categories:(NSArray*) categories 
     keywords:(NSArray*) keywords 
     limit:(NSNumber*) limit 
     skip:(NSNumber*) skip 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    


/**

 Send Clipped Email
 Send email

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 

 return type: NSObject*
 */
-(NSNumber*) sendUserClippedEmailWithCompletionBlock :(NSString*) xAuthToken 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Get Clipped Summary
 User clip summary

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 

 return type: NSObject*
 */
-(NSNumber*) getUserClippedSummaryWithCompletionBlock :(NSString*) xAuthToken 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Get History
 User activity history

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 

 return type: NSObject*
 */
-(NSNumber*) getUserHistoryWithCompletionBlock :(NSString*) xAuthToken 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Login
 User authentication

 @param username Loyalty id or phone number
 @param password clear-text password
 @param masterKeySecret admin override password
 

 return type: NSObject*
 */
-(NSNumber*) loginWithCompletionBlock :(NSString*) username 
     password:(NSString*) password 
     masterKeySecret:(NSString*) masterKeySecret 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Update Password
 Update Password

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param current Current password
 @param updated Updated password
 

 return type: NSObject*
 */
-(NSNumber*) updateUserPasswordWithCompletionBlock :(NSString*) xAuthToken 
     current:(NSString*) current 
     updated:(NSString*) updated 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Forgot Password
 Forgot Password

 @param origin a URI indicating the server from which the request initiated
 @param email User email address
 

 return type: NSObject*
 */
-(NSNumber*) forgotUserPasswordWithCompletionBlock :(NSString*) origin 
     email:(NSString*) email 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Reset Password
 Reset Password

 @param origin a URI indicating the server from which the request initiated
 @param resetToken Valid reset token
 @param updated Updated password
 

 return type: NSObject*
 */
-(NSNumber*) resetUserPasswordWithCompletionBlock :(NSString*) origin 
     resetToken:(NSString*) resetToken 
     updated:(NSString*) updated 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Register
 User registration

 @param origin a URI indicating the server from which the request initiated
 @param username Loyalty id or phone number
 @param password clear-text password
 @param email User email address
 

 return type: NSObject*
 */
-(NSNumber*) registrationWithCompletionBlock :(NSString*) origin 
     username:(NSString*) username 
     password:(NSString*) password 
     email:(NSString*) email 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    



@end
