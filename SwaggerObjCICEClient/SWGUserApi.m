#import "SWGUserApi.h"
#import "SWGFile.h"
#import "SWGQueryParamCollection.h"


@interface SWGUserApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation SWGUserApi

static NSString * basePath = @"http://ice-spartan.test.dpn.inmar.com";

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        self.apiClient = [SWGApiClient sharedClientFromPool:basePath];
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        if (apiClient) {
            self.apiClient = apiClient;
        }
        else {
            self.apiClient = [SWGApiClient sharedClientFromPool:basePath];
        }
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(SWGUserApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static SWGUserApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[SWGUserApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

+(void) setBasePath:(NSString*)path {
    basePath = path;
}

+(NSString*) getBasePath {
    return basePath;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [SWGApiClient requestQueueSize];
}


/*!
 * Get User Info
 * Access the information concerning the user account
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \returns NSObject*
 */
-(NSNumber*) getUserInfoWithCompletionBlock: (NSString*) xAuthToken
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getUserInfo");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"GET" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ? [[NSString  alloc]initWithString: data] : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Update User Info
 * Update user information
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param username Loyalty id or phone number
 * \param email User email address
 * \param firstName first name
 * \param lastName last name
 * \param phone telephone number
 * \param address1 address line one
 * \param address2 address line two
 * \param city address city
 * \param state address state
 * \param zip postal code
 * \param emailoptin true if user has opted in for email offers
 * \param altid Alternate identifier
 * \returns NSObject*
 */
-(NSNumber*) saveUserInfoWithCompletionBlock: (NSString*) xAuthToken
         username: (NSString*) username
         email: (NSString*) email
         firstName: (NSString*) firstName
         lastName: (NSString*) lastName
         phone: (NSString*) phone
         address1: (NSString*) address1
         address2: (NSString*) address2
         city: (NSString*) city
         state: (NSString*) state
         zip: (NSString*) zip
         emailoptin: (NSString*) emailoptin
         altid: (NSString*) altid
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling saveUserInfo");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    
    formParams[@"username"] = username;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"email"] = email;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"firstName"] = firstName;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"lastName"] = lastName;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"phone"] = phone;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"address1"] = address1;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"address2"] = address2;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"city"] = city;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"state"] = state;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"zip"] = zip;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"emailoptin"] = emailoptin;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"altid"] = altid;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"POST" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ?  data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Get Clipped Offers
 * Get the clipped offers for a user. Essentially the user's digital coupon 'wallet'.
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param sort Comma separated list of keys to sort by. \n\n**Offer Attribute Sorting:**\nKeys include: `brand`, `clipStartDate`, `clipEndDate`, `value` and `type`. \n\nThese keys sort ascending by default. To sort descending place a dash (minus sign) - before the attribute, like: `sort=value,-type`. This would sort offers by value ascending and type descending. \n\n**Special Sorting:** \nThese provide shortcuts or advanced sorting capabilities. Keys include:\n\n`popular` sort offers by popularity descending (most popular offers first) \n`asc` sort offers oldest to newest \n`desc` sort offers newest to oldest \n\nThese keys sort in a fixed direction. No flags (descending dashes, etc) are needed.
 * \param categories Comma separated list of category keys to filter offers by. \nTo get available category keys, see [Offers - Get Categories](#api-Offers-Get_Categories).
 * \param keywords Comma separated list of keywords to filter offers by. \nTo get available keywords, see [Offers - Get Keywords](#api-Offers-Get_Keywords).
 * \param limit Limit the number of returned results
 * \param skip Skip specific number of offers to return. Used with limit to provide pagination.
 * \returns NSArray*
 */
-(NSNumber*) getUserClippedOffersWithCompletionBlock: (NSString*) xAuthToken
         sort: (NSArray*) sort
         categories: (NSArray*) categories
         keywords: (NSArray*) keywords
         limit: (NSNumber*) limit
         skip: (NSNumber*) skip
        
        completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getUserClippedOffers");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/clipped", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(sort != nil) {
        
        queryParams[@"sort"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: sort format: @"csv"];
        
        
    }
    if(categories != nil) {
        
        queryParams[@"categories"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: categories format: @"csv"];
        
        
    }
    if(keywords != nil) {
        
        queryParams[@"keywords"] = [[SWGQueryParamCollection alloc] initWithValuesAndFormat: keywords format: @"csv"];
        
        
    }
    if(limit != nil) {
        
        queryParams[@"limit"] = limit;
    }
    if(skip != nil) {
        
        queryParams[@"skip"] = skip;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    
    // response is in a container
        // array container response type
    return [self.apiClient dictionary: requestUrl 
                       method: @"GET" 
                  queryParams: queryParams 
                         body: bodyDictionary 
                 headerParams: headerParams
                 authSettings: authSettings
           requestContentType: requestContentType
          responseContentType: responseContentType
              completionBlock: ^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionBlock(nil, error);
                    return;
                }
                
                if([data isKindOfClass:[NSArray class]]){
                    NSMutableArray * objs = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary* dict in (NSArray*)data) {
                        
                        NSObject* d = [dict copy];
                        
                        
                        [objs addObject:d];
                    }
                    completionBlock((NSArray*)objs, nil);
                }
                
                
            }];
    


    

    
}

/*!
 * Send Clipped Email
 * Send email
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \returns NSObject*
 */
-(NSNumber*) sendUserClippedEmailWithCompletionBlock: (NSString*) xAuthToken
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling sendUserClippedEmail");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/clipped/email", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"POST" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ?  data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Get Clipped Summary
 * User clip summary
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \returns NSObject*
 */
-(NSNumber*) getUserClippedSummaryWithCompletionBlock: (NSString*) xAuthToken
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getUserClippedSummary");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/clipped/summary", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"GET" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ?  data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Get History
 * User activity history
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \returns NSObject*
 */
-(NSNumber*) getUserHistoryWithCompletionBlock: (NSString*) xAuthToken
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling getUserHistory");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/history", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"GET" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ?  data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Login
 * User authentication
 * \param username Loyalty id or phone number
 * \param password clear-text password
 * \param masterKeySecret admin override password
 * \returns NSObject*
 */
-(NSNumber*) loginWithCompletionBlock: (NSString*) username
         password: (NSString*) password
         masterKeySecret: (NSString*) masterKeySecret
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'username' is set
    NSAssert(username != nil, @"Missing the required parameter `username` when calling login");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/login", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    
    formParams[@"username"] = username;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"password"] = password;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"masterKeySecret"] = masterKeySecret;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"POST" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ? data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Update Password
 * Update Password
 * \param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 * \param current Current password
 * \param updated Updated password
 * \returns NSObject*
 */
-(NSNumber*) updateUserPasswordWithCompletionBlock: (NSString*) xAuthToken
         current: (NSString*) current
         updated: (NSString*) updated
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'xAuthToken' is set
    NSAssert(xAuthToken != nil, @"Missing the required parameter `xAuthToken` when calling updateUserPassword");
    
    // verify the required parameter 'current' is set
    NSAssert(current != nil, @"Missing the required parameter `current` when calling updateUserPassword");
    
    // verify the required parameter 'updated' is set
    NSAssert(updated != nil, @"Missing the required parameter `updated` when calling updateUserPassword");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/password", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(xAuthToken != nil)
        headerParams[@"x-auth-token"] = xAuthToken;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    
    formParams[@"current"] = current;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"updated"] = updated;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"POST" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ? data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Forgot Password
 * Forgot Password
 * \param origin a URI indicating the server from which the request initiated
 * \param email User email address
 * \returns NSObject*
 */
-(NSNumber*) forgotUserPasswordWithCompletionBlock: (NSString*) origin
         email: (NSString*) email
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'origin' is set
    NSAssert(origin != nil, @"Missing the required parameter `origin` when calling forgotUserPassword");
    
    // verify the required parameter 'email' is set
    NSAssert(email != nil, @"Missing the required parameter `email` when calling forgotUserPassword");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/password/forgot", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(origin != nil)
        headerParams[@"Origin"] = origin;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    
    formParams[@"email"] = email;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"POST" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ? data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Reset Password
 * Reset Password
 * \param origin a URI indicating the server from which the request initiated
 * \param resetToken Valid reset token
 * \param updated Updated password
 * \returns NSObject*
 */
-(NSNumber*) resetUserPasswordWithCompletionBlock: (NSString*) origin
         resetToken: (NSString*) resetToken
         updated: (NSString*) updated
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'origin' is set
    NSAssert(origin != nil, @"Missing the required parameter `origin` when calling resetUserPassword");
    
    // verify the required parameter 'resetToken' is set
    NSAssert(resetToken != nil, @"Missing the required parameter `resetToken` when calling resetUserPassword");
    
    // verify the required parameter 'updated' is set
    NSAssert(updated != nil, @"Missing the required parameter `updated` when calling resetUserPassword");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/password/reset", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(origin != nil)
        headerParams[@"Origin"] = origin;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    
    formParams[@"resetToken"] = resetToken;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"updated"] = updated;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"POST" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ?  data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}

/*!
 * Register
 * User registration
 * \param origin a URI indicating the server from which the request initiated
 * \param username Loyalty id or phone number
 * \param password clear-text password
 * \param email User email address
 * \returns NSObject*
 */
-(NSNumber*) registrationWithCompletionBlock: (NSString*) origin
         username: (NSString*) username
         password: (NSString*) password
         email: (NSString*) email
        
        completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock
         {

    
    // verify the required parameter 'origin' is set
    NSAssert(origin != nil, @"Missing the required parameter `origin` when calling registration");
    
    // verify the required parameter 'username' is set
    NSAssert(username != nil, @"Missing the required parameter `username` when calling registration");
    
    // verify the required parameter 'password' is set
    NSAssert(password != nil, @"Missing the required parameter `password` when calling registration");
    
    // verify the required parameter 'email' is set
    NSAssert(email != nil, @"Missing the required parameter `email` when calling registration");
    

    NSMutableString* requestUrl = [NSMutableString stringWithFormat:@"%@/user/registration", basePath];

    // remove format in URL if needed
    if ([requestUrl rangeOfString:@".{format}"].location != NSNotFound)
        [requestUrl replaceCharactersInRange: [requestUrl rangeOfString:@".{format}"] withString:@".json"];

    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    if(origin != nil)
        headerParams[@"Origin"] = origin;
    
    
    // HTTP header `Accept` 
    headerParams[@"Accept"] = [SWGApiClient selectHeaderAccept:@[]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [SWGApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];
    
    id bodyDictionary = nil;
    
    

    NSMutableDictionary * formParams = [[NSMutableDictionary alloc]init];

    
    
    formParams[@"username"] = username;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"password"] = password;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    
    formParams[@"email"] = email;
    
    if(bodyDictionary == nil) {
      bodyDictionary = [[NSMutableArray alloc] init];
    }
    [bodyDictionary addObject:formParams];
    
    

    

    

    
    // non container response

    
    // primitive response
            // primitive response type
    return [self.apiClient stringWithCompletionBlock: requestUrl 
                                              method: @"POST" 
                                         queryParams: queryParams 
                                                body: bodyDictionary 
                                        headerParams: headerParams
                                        authSettings: authSettings
                                  requestContentType: requestContentType
                                 responseContentType: responseContentType
                                     completionBlock: ^(NSString *data, NSError *error) {
                         if (error) {
                             completionBlock(nil, error);
                             return;
                         }
                        NSObject *result = data ? data : nil;
                        completionBlock(result, nil);
                     }];
    
    
    


    
    // complex response
        

    

    
}



@end



