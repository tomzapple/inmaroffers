#import <Foundation/Foundation.h>
#import "SWGObject.h"
#import "SWGApiClient.h"


@interface SWGOffersApi: NSObject

@property(nonatomic, assign)SWGApiClient *apiClient;

-(instancetype) initWithApiClient:(SWGApiClient *)apiClient;
-(void) addHeader:(NSString*)value forKey:(NSString*)key;
-(unsigned long) requestQueueSize;
+(SWGOffersApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key;
+(void) setBasePath:(NSString*)basePath;
+(NSString*) getBasePath;
/**

 Get Offers
 General offer query. Useful for sorting offers on their attributes, categories and keywords. Also suitable for anonymous users since it does not rely on the user context or analytics.

 @param xAuthToken Authenticated session hash. See [User - Login](#api-User-Login)
 @param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 @param sort Comma separated list of keys to sort by. \n\n**Offer Attribute Sorting:**\nKeys include: `brand`, `clipStartDate`, `clipEndDate`, `value` and `type`. \n\nThese keys sort ascending by default. To sort descending place a dash (minus sign) - before the attribute, like: `sort=value,-type`. This would sort offers by value ascending and type descending. \n\n**Special Sorting:** \nThese provide shortcuts or advanced sorting capabilities. Keys include:\n\n`popular` sort offers by popularity descending (most popular offers first) \n`asc` sort offers oldest to newest \n`desc` sort offers newest to oldest \n\nThese keys sort in a fixed direction. No flags (descending dashes, etc) are needed.
 @param categories Comma separated list of category keys to filter offers by. \nTo get available category keys, see [Offers - Get Categories](#api-Offers-Get_Categories).
 @param keywords Comma separated list of keywords to filter offers by. \nTo get available keywords, see [Offers - Get Keywords](#api-Offers-Get_Keywords).
 @param limit Limit the number of returned results
 @param skip Skip specific number of offers to return. Used with limit to provide pagination.
 

 return type: NSArray*
 */
-(NSNumber*) getGroupOffersWithCompletionBlock :(NSString*) xAuthToken 
     groups:(NSArray*) groups 
     sort:(NSArray*) sort 
     categories:(NSArray*) categories 
     keywords:(NSArray*) keywords 
     limit:(NSNumber*) limit 
     skip:(NSNumber*) skip 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    


/**

 Get Campaign Offers
 Specialized query for campaign context analytic ranked offers.

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param campaign Campaign identifier
 @param availability Specialized parameter
 @param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 @param skip Skip specific number of offers to return. Used with limit to provide pagination.
 @param limit Limit the number of returned results
 

 return type: NSArray*
 */
-(NSNumber*) getCampaignOffersWithCompletionBlock :(NSString*) xAuthToken 
     campaign:(NSString*) campaign 
     availability:(NSNumber*) availability 
     groups:(NSArray*) groups 
     skip:(NSNumber*) skip 
     limit:(NSNumber*) limit 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    


/**

 Get Categories
 Get categories for available offers.

 @param xAuthToken Authenticated session hash. See [User - Login](#api-User-Login)
 

 return type: NSArray*
 */
-(NSNumber*) getOfferCategoriesWithCompletionBlock :(NSString*) xAuthToken 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    


/**

 Clip Offer
 Clip an offer.

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param _id Offer Id
 

 return type: NSObject*
 */
-(NSNumber*) clipOfferWithCompletionBlock :(NSString*) xAuthToken 
     _id:(NSNumber*) _id 
    
    completionHandler: (void (^)(NSObject* output, NSError* error))completionBlock;
    


/**

 Get Email Offers
 Specialized query for 'email' context analytic ranked offers.

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param runId Specialized parameter
 @param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 @param skip Skip specific number of offers to return. Used with limit to provide pagination.
 @param limit Limit the number of returned results
 

 return type: NSArray*
 */
-(NSNumber*) getEmailOffersWithCompletionBlock :(NSString*) xAuthToken 
     runId:(NSNumber*) runId 
     groups:(NSArray*) groups 
     skip:(NSNumber*) skip 
     limit:(NSNumber*) limit 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    


/**

 Get Keywords
 Get offer keywords.

 @param xAuthToken Authenticated session hash. See [User - Login](#api-User-Login)
 

 return type: NSArray*
 */
-(NSNumber*) getOfferKeywordsWithCompletionBlock :(NSString*) xAuthToken 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    


/**

 Get Ranked Offers
 Get analytic ranked offers ordered by analytic `contextTypes` and grouped by state `groups`.

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param campaign Campaign identifier
 @param contextTypes Comma separated list of analytic context types to return in that order
 @param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 @param skip Skip specific number of offers to return. Used with limit to provide pagination.
 @param limit Limit the number of returned results
 

 return type: NSArray*
 */
-(NSNumber*) getRankedOffersWithCompletionBlock :(NSString*) xAuthToken 
     campaign:(NSString*) campaign 
     contextTypes:(NSArray*) contextTypes 
     groups:(NSArray*) groups 
     skip:(NSNumber*) skip 
     limit:(NSNumber*) limit 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    


/**

 Get Relevant Offers
 Specialized query for 'relevant' context analytic ranked offers.

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param relevancyType Specialized parameter
 @param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 @param skip Skip specific number of offers to return. Used with limit to provide pagination.
 @param limit Limit the number of returned results
 

 return type: NSArray*
 */
-(NSNumber*) getRelevantOffersWithCompletionBlock :(NSString*) xAuthToken 
     relevancyType:(NSString*) relevancyType 
     groups:(NSArray*) groups 
     skip:(NSNumber*) skip 
     limit:(NSNumber*) limit 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    


/**

 Get Targeted Offers
 Specialized query for 'targeted' context analytic ranked offers.

 @param xAuthToken Authenticated session hash (Required). See [User - Login](#api-User-Login)
 @param runId Specialized parameter
 @param groups Comma separated list of offer states to group offers by. Requires authenticated user.
 @param skip Skip specific number of offers to return. Used with limit to provide pagination.
 @param limit Limit the number of returned results
 

 return type: NSArray*
 */
-(NSNumber*) getTargetedOffersWithCompletionBlock :(NSString*) xAuthToken 
     runId:(NSNumber*) runId 
     groups:(NSArray*) groups 
     skip:(NSNumber*) skip 
     limit:(NSNumber*) limit 
    
    completionHandler: (void (^)(NSArray* output, NSError* error))completionBlock;
    



@end
