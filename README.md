# InmarOffers README #
### [Promo Video](https://youtu.be/pNknWVDbCJw) ###
### What is InMar Offers? ###

A proximity marketing application that can be used by retailers to push offers or coupons to their customers.
This application uses Inmar's ICE 2.0 API and DeCA API to fetch coupons and push it to the user.


[]()![Screen Shot 2015-08-06 at 4.48.52 pm.png](https://bitbucket.org/repo/a4qjbp/images/4011209726-Screen%20Shot%202015-08-06%20at%204.48.52%20pm.png)


### Beacon Configuration ###

* The beacons can be configured to respond to any categories that come up on the ICE API. The beacons can be configured by editing the **Beacons.plist** file located in the source code. This beacon configuration could well be done by a web service in the future.

![Screen Shot 2015-08-06 at 4.58.33 pm.png](https://bitbucket.org/repo/a4qjbp/images/1483520054-Screen%20Shot%202015-08-06%20at%204.58.33%20pm.png)

The **beaconMap** dictionary is the mapping between the beacon's MAC address and the Beacon Name. This could alternatively fetched from the Footmarks API when we start using their rest API.

The **offerMap** dictionary is the mapping between the beacon's and the offer categories(ICE). This can be modified too.

### Build InMarOffers ###
A few steps to get Inmar Offers to build and Run

1. Download the code from github/bitbucket.
2. Open the InmarOffers.xcworkspace file. This workspace will cause it to build all the Pods dependancies as well. 
3. Connect an iOS device and with the target as InmarOffers build and run the application. It is very important to note that this application will not compile on the simulator as it has a dependancy on the Footmarks SDK which runs into an error if you build it on the Simulator.

## Troubleshooting ##
* If the code has compilation errors which point to Pods follow the below instructions
1. Quit Xcode
2. Delete the Xcode's derived data folder for the app
3. Delete **InmarOffers.xcworkspace**, **Podfile.lock**, and **Pods** folder
4. On Terminal cd to the root project folder and run "pod install"
5. Open Xcode and Build the app. This should fix any Pod related problems


### Who do I talk to? ###

* thomsonvarghese2005@gmail.com